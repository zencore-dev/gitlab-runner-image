FROM dtzar/helm-kubectl as kubectl
FROM registry.gitlab.com/gitlab-org/terraform-images/releases/1.3 as terraform

FROM google/cloud-sdk:406.0.0 as gcloud
COPY --from=kubectl /usr/local/bin/helm /usr/local/bin/helm
COPY --from=kubectl /usr/local/bin/kubectl /usr/local/bin/kubectl
COPY --from=terraform /usr/bin/gitlab-terraform /usr/bin/gitlab-terraform
COPY --from=terraform /usr/local/bin/terraform /usr/local/bin/terraform

RUN apt update -q && \
    apt install -qy idn2 jq &&\
    kubectl version --client && \
    helm version && \
    gitlab-terraform version
